## Teste Desenvolvedor Full Stack

O teste consiste em criar uma aplicação com Backend(NodeJS) que expõe uma API REST de um CRUD de usuários e filmes e uma aplicação web contendo uma interface(Angular) para login e acesso a dados de uma API externa.

# Back-end
    •  Todos os endpoints de consulta de dados devem ter autenticação por Token ou similar

# Front-end
O front-end deverá ser desenvolvido em Angular deve apresentar pelo menos os seguintes requisitos:
    •  Interface de login
    •  Feedbacks de usuário ou senha incorreta
    •  Listagem dos dados de filmes
    •  Paginação dos dados
    •  Listagem dos dados de Usuários

# Critérios de avaliação
    •  Funcionamento do projeto
    •  Estrutura do código
    •  Uso de boas práticas
    •  Cumprimento dos requisitos mínimos

# Deve ser entregue:
    •  Um repositório git (fork deste)
    •  Criação de um Readme com instruções de build

Não se deve fazer o commit de pastas como node_modules, o projeto deve instalar suas dependências a partir do package.json

# Extras:
    •  Publicação no Vercel.app
    •  Uso de Containers Docker
    •  Uso de Testes
    •  Build para produção


# Backend (Node.js v14.17.0):
Instalação das dependências:

npm install

Após a instalação das dependências e a configuração do banco de dados, execute node index.js para iniciar o servidor.
O backend estará acessível em http://localhost:3000.
Endpoints:

O backend oferece os seguintes endpoints:
    • POST /auth/register: Registra um novo usuário.
    • POST /auth/login: Autentica um usuário.
    • GET /movies: Retorna todos os filmes.
    • GET /users: Retorna todos os usuários.
    • GET /users/:id: Retorna um usuário específico pelo ID.
    • PUT /users/:id: Atualiza um usuário existente pelo ID.
    • DELETE /users/:id: Exclui um usuário pelo ID.

# Frontend:
Instalação das dependências:

Certifique-se de ter o Node.js v14.17.0 instalado.
Navegue até o diretório do frontend (/frontend) no terminal.
Execute npm install para instalar todas as dependências.
Configuração do ambiente:

Verifique se o arquivo environment.ts contém as configurações corretas, como a URL do backend.
Execução do aplicativo:

Após a instalação das dependências e a configuração do ambiente, execute ng serve para iniciar o servidor de desenvolvimento.
O frontend estará acessível em http://localhost:4200.
Funcionalidades do Frontend:

O frontend consome os endpoints do backend para exibir uma lista de usuários e filmes.
A interface de usuário permite visualizar, editar e excluir usuários.
Os usuários podem se registrar e fazer login para acessar recursos protegidos.