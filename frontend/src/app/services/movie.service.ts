// movie.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private baseURL = 'http://localhost:3000/movies'; // URL do backend

  constructor(private http: HttpClient) { }

  // Método para obter a lista de filmes do backend
  getMovies() {
    return this.http.get(this.baseURL);
  }
}
