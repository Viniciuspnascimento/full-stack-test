import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { MovieListComponent } from './components/movie-list/movie-list.component';
import { AuthGuard } from './guards/auth.guard';
import { UserRegisterComponent } from './components/user-register/user-register.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },

  { path: 'users', component: UserListComponent, canActivate: [AuthGuard] },
  { path: 'register', component: UserRegisterComponent, canActivate: [AuthGuard] },
  { path: 'movies', component: MovieListComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
