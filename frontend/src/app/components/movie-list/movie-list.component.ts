import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  movies: any[] = [];
  rows: any[][] = [];
  moviesPerPage: number = 10; // 2 rows * 5 columns
  currentPage: number = 1;
  totalPages: number;

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.loadMovies();
  }

  loadMovies() {
    this.movieService.getMovies().subscribe((movies: any[]) => {
      this.movies = movies;
      this.totalPages = Math.ceil(this.movies.length / this.moviesPerPage);
      this.generateRows();
    });
  }

  generateRows() {
    const startIdx = (this.currentPage - 1) * this.moviesPerPage;
    const endIdx = startIdx + this.moviesPerPage;
    this.rows = [];
    for (let i = startIdx; i < endIdx && i < this.movies.length; i += 5) {
      this.rows.push(this.movies.slice(i, i + 5));
    }
  }

  nextPage() {
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.generateRows();
    }
  }

  prevPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.generateRows();
    }
  }

  getRows() {
    return this.rows;
  }
}
