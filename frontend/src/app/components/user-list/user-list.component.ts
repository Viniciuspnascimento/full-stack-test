import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: any[] = [];
  selectedUser: any = null;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers() {
    this.userService.getAllUsers().subscribe(users => {
      this.users = users;
    }, error => {
      console.error('Erro ao carregar usuários', error);
    });
  }

  editUser(user: any) {
    this.selectedUser = { ...user }; // Clone the user object
  }

  deleteUser(user: any) {
    this.userService.deleteUser(user.id).subscribe(() => {
      console.log('Usuário excluído com sucesso!');
      this.loadUsers();
    }, error => {
      console.error('Erro ao excluir usuário', error);
    });
  }

  updateUser() {
    if (this.selectedUser) {
      this.userService.updateUser(this.selectedUser.id, this.selectedUser).subscribe(() => {
        console.log('Usuário atualizado com sucesso!');
        this.loadUsers();
        this.selectedUser = null;
      }, error => {
        console.error('Erro ao atualizar usuário', error);
      });
    }
  }


  cancelEdit() {
    this.selectedUser = null;
  }
}
