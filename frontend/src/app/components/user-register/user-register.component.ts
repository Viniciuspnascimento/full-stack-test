import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  name: string = '';
  email: string = '';
  password: string = '';
  errorMessage: string = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() { }

  register(event: Event): void {
    event.preventDefault();
    if (!this.name || !this.email || !this.password) {
      this.errorMessage = 'Todos os campos são obrigatórios!';
      return;
    }

    const user = { name: this.name, email: this.email, password: this.password };
    this.authService.register(user)
      .subscribe(
        () => {
          console.log('Usuário registrado com sucesso!');
          this.errorMessage = '';
          this.name = '';
          this.email = '';
          this.password = '';
          this.router.navigate(['/login']);
        },
        (error) => {
          if (error.status === 409) { // Supondo que 409 é o código de status para "Conflito" quando o email já está registrado
            this.errorMessage = 'Email já cadastrado!';
          } else {
            this.errorMessage = 'Erro ao registrar. Tente novamente mais tarde.';
          }
        }
      );
  }
}
