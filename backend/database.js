// database.js
// database.js
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./database.db', err => {
  if (err) {
    console.error('Erro ao conectar ao banco de dados:', err.message);
  } else {
    console.log('Conexão bem-sucedida com o banco de dados SQLite');

    // Criação da tabela de usuários
    db.run(`
      CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        email TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL
      )
    `);

    // Criação da tabela de filmes com a nova coluna poster_path
    db.run(`
      CREATE TABLE IF NOT EXISTS movies (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        director TEXT NOT NULL,
        year INTEGER NOT NULL,
        poster_path TEXT
      )
    `, (err) => {
      if (err) {
        console.error('Erro ao criar a tabela de filmes:', err.message);
      } else {
        // Adiciona a coluna poster_path se ela não existir
        db.run(`
          ALTER TABLE movies ADD COLUMN poster_path TEXT
        `, (err) => {
          if (err && err.message.includes('duplicate column name')) {
            console.log('A coluna poster_path já existe.');
          } else if (err) {
            console.error('Erro ao adicionar a coluna poster_path:', err.message);
          }
        });
      }
    });
  }
});

module.exports = db;

