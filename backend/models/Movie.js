const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./database.db');

class Movie {
  static getAllMovies() {
    return new Promise((resolve, reject) => {
      const query = 'SELECT * FROM movies';
      db.all(query, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  static createMovie(title, director, year, poster_path) {
    return new Promise((resolve, reject) => {
      const query = 'INSERT INTO movies (title, director, year, poster_path) VALUES (?, ?, ?, ?)';
      db.run(query, [title, director, year, poster_path], function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: this.lastID });
        }
      });
    });
  }

  static updateMovie(movieId, title, director, year, poster_path) {
    return new Promise((resolve, reject) => {
      const query = 'UPDATE movies SET title = ?, director = ?, year = ?, poster_path = ? WHERE id = ?';
      db.run(query, [title, director, year, poster_path, movieId], function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: movieId });
        }
      });
    });
  }

  static deleteMovie(movieId) {
    return new Promise((resolve, reject) => {
      const query = 'DELETE FROM movies WHERE id = ?';
      db.run(query, [movieId], function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: movieId });
        }
      });
    });
  }
}

module.exports = Movie;