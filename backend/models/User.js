const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./database.db');

class User {
  static getUserByEmail(email) {
    return new Promise((resolve, reject) => {
      const query = 'SELECT * FROM users WHERE email = ?';
      db.get(query, [email], (err, row) => {
        if (err) {
          reject(err);
        } else {
          resolve(row);
        }
      });
    });
  }

  static createUser(name, email, password) {
    return new Promise((resolve, reject) => {
      const query = 'INSERT INTO users (name, email, password) VALUES (?, ?, ?)';
      db.run(query, [name, email, password], function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: this.lastID, name, email });
        }
      });
    });
  }

  static getAllUsers() {
    return new Promise((resolve, reject) => {
      const query = 'SELECT * FROM users';
      db.all(query, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  static getUserById(id) {
    return new Promise((resolve, reject) => {
      const query = 'SELECT * FROM users WHERE id = ?';
      db.get(query, [id], (err, row) => {
        if (err) {
          reject(err);
        } else {
          resolve(row);
        }
      });
    });
  }

  static deleteUser(userId) {
    return new Promise((resolve, reject) => {
      const query = 'DELETE FROM users WHERE id = ?';
      db.run(query, [userId], function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: userId });
        }
      });
    });
  }
  
  static updateUser(userId, name, email, password) {
    return new Promise((resolve, reject) => {
      const query = 'UPDATE users SET name = ?, email = ?, password = ? WHERE id = ?';
      db.run(query, [name, email, password, userId], function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: userId });
        }
      });
    });
  }
  
}

module.exports = User;
