// movieSeeder.js

const fetch = require('node-fetch');
const Movie = require('../models/Movie');

const apiKey = '74ebf45faa52b6149556ebe9d58bbaf3';
const apiUrl = `https://api.themoviedb.org/3/discover/movie?api_key=${apiKey}`;

async function fetchMovies() {
  try {
    const response = await fetch(apiUrl);
    const data = await response.json();
    return data.results;
  } catch (error) {
    console.error('Error fetching movies:', error);
    throw error;
  }
}

async function populateMovies() {
  try {
    const movies = await fetchMovies();
    for (const movie of movies) {
      const { title, director, release_date } = movie;
      const year = new Date(release_date).getFullYear();
      await Movie.createMovie(title, director, year);
    }
    console.log('Movies populated successfully.');
  } catch (error) {
    console.error('Error populating movies:', error);
  }
}

populateMovies();
