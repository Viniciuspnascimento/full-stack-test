// middleware/auth.js

const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  const token = req.headers['authorization'];

  if (!token) {
    return res.status(403).send({ auth: false, message: 'Token not provided.' });
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    }

    // Se a autenticação for bem-sucedida, armazene o ID do usuário decodificado na solicitação para uso posterior
    req.userId = decoded.id;
    next();
  });
}

module.exports = verifyToken;
