// // index.js
// const express = require('express');
// const bodyParser = require('body-parser');
// const authRoutes = require('./routes/auth');
// const movieRoutes = require('./routes/movie');
// const db = require('./database');

// const app = express();
// const PORT = process.env.PORT || 3000;

// app.use(bodyParser.json());

// app.use('/auth', authRoutes);
// app.use('/movies', movieRoutes);

// db.on('open', () => {
//   app.listen(PORT, () => {
//     console.log(`Server is running on http://localhost:${PORT}`);
//   });
// });



// index.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const authRoutes = require('./routes/auth');
const movieRoutes = require('./routes/movie');
const db = require('./database');
const fetch = require('node-fetch'); // Importe a biblioteca node-fetch

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware para parsear o corpo das requisições
app.use(bodyParser.json());

// Middleware para habilitar CORS
app.use(cors());

// Rotas de autenticação
app.use('/auth', authRoutes);

// Rotas de filmes
app.use('/movies', movieRoutes);

// Função para popular o banco de dados de filmes
async function populateMovies() {
  try {
    // Faça uma solicitação para a API de filmes para obter os catálogos de filmes
    const response = await fetch('https://api.themoviedb.org/3/discover/movie?api_key=74ebf45faa52b6149556ebe9d58bbaf3');
    const data = await response.json();

    // Verifique se os dados foram retornados com sucesso
    if (data && data.results) {
      // Limpe a tabela de filmes antes de inserir novos dados (opcional)
      await new Promise((resolve, reject) => {
        db.run('DELETE FROM movies', (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });

      // Itere sobre os resultados e insira cada filme no banco de dados
      for (const movieData of data.results) {
        const { title, release_date, poster_path } = movieData;
        const director = 'Unknown'; // A API não fornece diretamente o diretor do filme
        const year = new Date(release_date).getFullYear();
        const fullPosterPath = `https://image.tmdb.org/t/p/w500${poster_path}`;

        // Insira o filme no banco de dados
        await new Promise((resolve, reject) => {
          db.run('INSERT INTO movies (title, director, year, poster_path) VALUES (?, ?, ?, ?)', [title, director, year, fullPosterPath], function (err) {
            if (err) {
              reject(err);
            } else {
              resolve();
            }
          });
        });
      }

      console.log('Banco de dados de filmes populado com sucesso!');
    } else {
      console.log('Não foi possível obter os catálogos de filmes da API.');
    }
  } catch (error) {
    console.error('Erro ao popular o banco de dados de filmes:', error);
  }
}

// Popula o banco de dados de filmes ao iniciar o servidor
db.on('open', async () => {
  await populateMovies(); // Popula o banco de dados de filmes
  app.listen(PORT, () => {
    console.log(`Servidor está rodando em http://localhost:${PORT}`);
  });
});
