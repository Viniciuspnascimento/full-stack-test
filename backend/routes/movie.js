// routes/movie.js
const express = require('express');
const router = express.Router();
const Movie = require('../models/Movie'); // Certifique-se de que está importando corretamente

// Rota para obter todos os filmes
router.get('/', async (req, res) => {
  try {
    const movies = await Movie.getAllMovies();
    res.status(200).send(movies);
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error.' });
  }
});

// Rota para criar um novo filme
router.post('/', async (req, res) => {
  const { title, director, year, poster_path } = req.body;

  try {
    const newMovie = await Movie.createMovie(title, director, year, poster_path);
    res.status(201).send(newMovie);
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error.' });
  }
});

// Rota para atualizar um filme existente
router.put('/:id', async (req, res) => {
  const { title, director, year, poster_path } = req.body;
  const movieId = req.params.id;

  try {
    const updatedMovie = await Movie.updateMovie(movieId, title, director, year, poster_path);
    res.status(200).send(updatedMovie);
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error.' });
  }
});

// Rota para excluir um filme
router.delete('/:id', async (req, res) => {
  const movieId = req.params.id;

  try {
    await Movie.deleteMovie(movieId);
    res.status(204).send();
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error.' });
  }
});

module.exports = router;
