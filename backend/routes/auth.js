// routes/auth.js

const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
// Rota para registro de usuário
router.post('/register', async (req, res) => {
  const { name, email, password } = req.body;

  try {
    // Verifica se o usuário já existe
    const existingUser = await User.getUserByEmail(email);
    if (existingUser) {
      return res.status(400).send({ message: 'User already exists.' });
    }

    // Criptografa a senha
    const hashedPassword = bcrypt.hashSync(password, 8);

    // Cria o usuário
    const newUser = await User.createUser(name, email, hashedPassword);
    res.status(201).send(newUser);
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error.' });
  }
});

// Rota para listar todos os usuários
router.get('/users', async (req, res) => {
  try {
    const users = await User.getAllUsers();
    res.status(200).json(users);
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Internal server error.' });
  }
});

// Rota para buscar um usuário específico por ID
router.get('/users/:id', async (req, res) => {
  const userId = req.params.id;

  try {
    const user = await User.getUserById(userId);
    if (!user) {
      return res.status(404).send({ message: 'User not found' });
    }
    res.status(200).send(user);
  } catch (error) {
    console.error('Error fetching user by ID:', error);
    res.status(500).send({ message: 'Internal server error' });
  }
});


// Rota para login de usuário
router.post('/login', async (req, res) => {
  const { email, password } = req.body;

  try {
    // Verifica se o usuário existe
    const user = await User.getUserByEmail(email);
    if (!user) {
      return res.status(404).send({ message: 'User not found' });
    }

    // Verifica se a senha está correta
    const isPasswordValid = bcrypt.compareSync(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).send({ message: 'Invalid password' });
    }

    // Se o usuário e a senha estiverem corretos, gera um token de autenticação JWT
    const token = jwt.sign({ id: user.id }, 'my_secret_key', { expiresIn: '1h' });

    // Retorna o token de autenticação
    res.status(200).send({ token });
  } catch (error) {
    console.error('Error authenticating user:', error);
    res.status(500).send({ message: 'Internal server error' });
  }
});

// Rota para deletar um usuário
router.delete('/users/:id', async (req, res) => {
  const userId = req.params.id;

  try {
    const deletedUser = await User.deleteUser(userId);
    res.status(200).send(deletedUser);
  } catch (error) {
    console.error('Erro ao deletar usuário:', error);
    res.status(500).send({ message: 'Erro interno do servidor' });
  }
});

// Rota para atualizar um usuário
router.put('/users/:id', async (req, res) => {
  const userId = req.params.id;
  const { name, email, password } = req.body;

  try {
    const updatedUser = await User.updateUser(userId, name, email, password);
    res.status(200).send(updatedUser);
  } catch (error) {
    console.error('Erro ao atualizar usuário:', error);
    res.status(500).send({ message: 'Erro interno do servidor' });
  }
});


module.exports = router;
